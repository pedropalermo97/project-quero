class StudentController < ApplicationController
  before_action :set_student, only:[:show, :update, :destroy]
  def create
    @student = Student.new(student_params)
    if @student.save
      render json: @student, status: 201
    else
      render json: @student.errors, status: 422
    end
  end

  def index
    @students=Student.all
    render json: @students
  end

  def show
    render json: @student
  end

  def update
    if @student.update(student_params)
      render json: @student
    else
      render json: @student.errors, status: 422
    end
  end

  def destroy
    @student.destroy
  end

  def set_student
    @student= Student.find(params[:id])
  end

  def student_params
    params.require(:student).permit(:name, :cpf, :gender, :payment_method)
  end

end



