class EnrollmentController < ApplicationController
  before_action :set_enrollment, only:[:show, :update, :destroy]
  
  def create
    @enrollment = Enrollment.new(enrollment_params)
    if @enrollment.save
      render json: @enrollment, status: 201
    else
      render json: @enrollment.errors, status: 422
    end
  end

  def index
    @enrollments=Enrollment.all
    render json: @enrollments
  end

  def show
    render json: @enrollment
  end

  def update
    if @enrollment.update(enrollment_params)
      render json: @enrollment
    else
      render json: @enrollment.errors, status: 422
    end
  end

  def destroy
    @enrollment.destroy
  end

  def set_enrollment
    @enrollment= Enrollment.find(params[:id])
  end

  def enrollment_params
    params.require(:enrollment).permit(:total_course_fee, :number_invoice, :expiration_day, :name_course, :student_id, :instituion_id)
  end

end

