class InvoiceController < ApplicationController
  before_action :set_invoice, only:[:show, :update, :destroy]
  def create
    @invoice = Invoice.new(invoice_params)
    if @invoice.save
      render json: @invoice, status: 201
    else
      render json: @invoice.errors, status: 422
    end
  end

  def index
    @invoices=Invoice.all
    render json: @invoices
  end

  def show
    render json: @invoice
  end

  def update
    if @invoice.update(invoice_params)
      render json: @invoice
    else
      render json: @invoice.errors, status: 422
    end
  end

  def destroy
    @invoice.destroy
  end

  def set_invoice
    @invoice= Invoice.find(params[:id])
  end

  def invoice_params
    params.require(:invoice).permit(:invoice_amount, :due_data, :status, :enrollment_id)
  end

end

