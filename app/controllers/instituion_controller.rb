class InstituionController < ApplicationController
  before_action :set_instituion, only:[:show, :update, :destroy]
  def create
    @instituion = Instituion.new(instituion_params)
    if @instituion.save
      render json: @instituion, status: 201
    else
      render json: @instituion.errors, status: 422
    end
  end

  def index
    @instituions=Instituion.all
    render json: @instituions
  end

  def show
    render json: @instituion
  end

  def update
    if @instituion.update(instituion_params)
      render json: @instituion
    else
      render json: @instituion.errors, status: 422
    end
  end

  def destroy
    @instituion.destroy
  end

  def set_instituion
    @instituion= Instituion.find(params[:id])
  end

  def instituion_params
    params.require(:instituion).permit(:name,:cnpj, :type_status)
  end

end

