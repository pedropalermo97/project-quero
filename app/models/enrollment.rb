class Enrollment < ApplicationRecord
  require 'date'

  belongs_to :student
  belongs_to :instituion
  has_many :invoices, dependent: :destroy
  after_create :create_invoice
  after_update :after_update_enrollment

  #Validation
  validates :total_course_fee, :number_invoice, :expiration_day, :name_course, presence: true
  validate :total_course_fee_more_than_0
  validate :number_invoice_more_than_0
  validate :expiration_day_between_1_31




  def total_course_fee_more_than_0
    if total_course_fee <= 0 
      errors.add(:total_course_fee, "Precisar ser maior que 0 o valor do curso")
    end
  end

  def number_invoice_more_than_0
    if number_invoice <= 0 
      errors.add(:number_invoice, "Precisar ser maior que 0 o valor do curso")
    end
  end

  def expiration_day_between_1_31
    if number_invoice <= 0 || number_invoice >= 31
      errors.add(:expiration_day, "Precisar ser maior que 0 o valor do curso")
    end
  end
  
  def create_invoice
      UseCaseInvoice.perform(self.total_course_fee, self.number_invoice, self.expiration_day, self)
  end

  def after_update_enrollment
    UseCaseInvoice.perform(self.total_course_fee, self.number_invoice, self.expiration_day, self)
  end

end
