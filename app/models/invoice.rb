class Invoice < ApplicationRecord
  belongs_to :enrollment

  #Validation
  validates :invoice_amount, :due_data, :status, presence: true
  after_initialize :init_status, if: :new_record?
  validates_inclusion_of :status, in: [ "atrasada","paga","aberta" ]
  
  def init_status
    self.status ||= "aberta" 
  end


end

  

