class Student < ApplicationRecord
    has_many :enrollments, dependent: :destroy
    #validation
    validates :name, :cpf, :gender, :payment_method, presence: true
    validates :name, uniqueness: true 
    validates :cpf, length: {is: 11}, numericality: {only_integer: true}
    #Utilizando Regex
    #VALIDATE_CPF =/[0~9]/
    #validates :cpf , format: {with: VALIDATE_CPF, message:"Insira apenas números"}
    validates :gender, inclusion: { in: %w(f m),
        message: "Não é uma entrada aceita para genêro" }
    validates :payment_method, inclusion: { in: %w(boleto cartao),
        message: "Escolha uma forma de pagamento :Cartao ou Boleto" }    
end
