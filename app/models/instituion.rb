class Instituion < ApplicationRecord
    has_many :enrollments, dependent: :destroy
    
    validates :name,:cnpj, presence: true
    validates :name,:cnpj, uniqueness: true 
    validates :cnpj, length: {is: 11}, numericality: {only_integer: true}
    validates :type_status, inclusion: { in: %w(universidade escola creche),
        message: "Entrada não aceita" }
        
end