class UseCaseInvoice 
    include UseCase

    def initialize( total_course_fee, number_invoice, expiration_day, enrollment)
        @total_course_fee = total_course_fee
        @number_invoice = number_invoice
        @expiration_day = expiration_day
        @enrollment = enrollment
    end

    def perform
        ActiveRecord::Base.transaction do
            create_invoice
            after_update_enrollment
        end
    end

    def create_invoice
        aux = expiration_day - Integer(Date.today.day)
        date = Date.today.next_day(aux)
        if(Date.today>date)
            date.next_month(1)
        end
        date.strftime('%d/%m/%Y')
        for a in 0..number_invoice-1 do
            @invoice = Invoice.create!(enrollment_id:enrollment.id,invoice_amount:total_course_fee/number_invoice,due_data: date.next_month(a) )
        end
    end

    def after_update_enrollment
        @invoice =  Invoice.where(enrollment_id:enrollment.id)
        @invoice.each do |p|
            p.destroy
        end
        aux = expiration_day - Integer(Date.today.day)
        date = Date.today.next_day(aux)
        if(Date.today>date)
            date.next_month(1)
        end
        date.strftime('%d/%m/%Y')
        for a in 0..number_invoice-1 do
            @invoice = Invoice.create!(enrollment_id:enrollment.id,invoice_amount:total_course_fee/number_invoice,due_data: date.next_month(a) )
        end

    end

    private

    attr_reader :enrollment, :number_invoice, :total_course_fee, :expiration_day

end