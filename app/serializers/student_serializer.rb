class StudentSerializer < ActiveModel::Serializer
  attributes  :id, :name, :cpf, :gender, :payment_method
  has_many :enrollments
end
