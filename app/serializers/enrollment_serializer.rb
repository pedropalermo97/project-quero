class EnrollmentSerializer < ActiveModel::Serializer
  attributes :id, :total_course_fee, :number_invoice, :expiration_day, :name_course
  has_many :invoices
end
