class InvoiceSerializer < ActiveModel::Serializer
  attributes :id, :invoice_amount, :due_data, :status, :enrollment_id
end
