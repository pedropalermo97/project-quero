class InstituionSerializer < ActiveModel::Serializer
  attributes :id, :id, :name, :cnpj, :type_status
  has_many :enrollments
end
