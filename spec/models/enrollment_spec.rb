require 'rails_helper'

RSpec.describe Enrollment, type: :model do
    instituion = Instituion.create(name: "Modelo", cnpj: "14345678911", type_status: "universidade")
    student = Student.create(name: "usuario", cpf: "10005678911", gender: "m", payment_method: "cartao")
    
    it 'has not completed faultes information' do
        enrollment = Enrollment.new(
            name_course: "ingles",
            number_invoice: 4,
            total_course_fee: 2000.0

        )
        expect(enrollment).to_not be_valid
    end

    it 'has completed' do
        enrollment = Enrollment.new(
            name_course: "ingles",
            total_course_fee: 2000.0,
            number_invoice: 1,
            expiration_day: 10,
            student_id:1,
            instituion_id:1	
            )
        expect(enrollment).to be_valid
    end

end