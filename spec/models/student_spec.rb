require 'rails_helper'

RSpec.describe Instituion, type: :model do
    it 'has not completed faultes information' do
        student = Student.new(
            name:"teste"
        )
        expect(student).to_not be_valid
    end

    it 'has completed' do
        student = Student.new(
            name: "teste",
	    	cpf: "10005678911",
		    gender: "m",
	        payment_method: "cartao"
            )
        expect(student).to be_valid
    end

    it 'has not completed validation cpf incorrect'  do
        student = Student.new(
            name: "teste",
	    	cpf: "p0005678911",
            gender: "m",
	        payment_method: "cartao"
        )
        expect(student).to_not be_valid
    end

    it 'has not completed validation gender incorrect'  do
        student = Student.new(
            name: "teste",
	    	cpf: "30005678911",
            gender: "a",
	        payment_method: "cartao"
        )
        expect(student).to_not be_valid
    end

    it 'has not completed validation payment method incorrect'  do
        student = Student.new(
            name: "teste",
	    	cpf: "40005678911",
            gender: "a",
	        payment_method: "teste1"
        )
        expect(student).to_not be_valid
    end
end