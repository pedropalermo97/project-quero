require 'rails_helper'

RSpec.describe Instituion, type: :model do
    it 'has not completed faultes information' do
        instituion = Instituion.new(
            name:"teste"
        )
        expect(instituion).to_not be_valid
    end

    it 'has completed' do
        instituion = Instituion.new(
            name: "teste",
		    cnpj: "12345678911",
		    type_status: "universidade"
            )
        expect(instituion).to be_valid
    end

    it 'has not completed validation cnpj incorrect'  do
        instituion = Instituion.new(
            name: "teste",
		    cnpj: "p2345678911",
		    type_status: "universidade"
            )
        expect(instituion).to_not be_valid
    end

    it 'has not completed validation type status incorrect'  do
        instituion = Instituion.new(
            name: "teste",
		    cnpj: "p2345678911",
		    type_status: "teste"
            )
        expect(instituion).to_not be_valid
    end

end