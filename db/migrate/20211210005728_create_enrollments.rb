class CreateEnrollments < ActiveRecord::Migration[6.1]
  def change
    create_table :enrollments do |t|
      t.decimal :total_course_fee
      t.integer :number_invoice
      t.integer :expiration_day
      t.string :name_course
      t.references :student, null: false, foreign_key: true
      t.references :instituion, null: false, foreign_key: true

      t.timestamps
    end
  end
end
