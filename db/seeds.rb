# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Student.create!(name:"Pedro",cpf:"16489301741",gender:"m",payment_method:"cartao",birth_date:"18/04/1997")
Student.create!(name:"Gustavo",cpf:"16489322255",gender:"m",payment_method:"boleto",birth_date:"12/04/2020")
Student.create!(name:"Ivan",cpf:"12345678999",gender:"m",payment_method:"cartao",birth_date:"8/11/2015")
Student.create!(name:"Rafaela",cpf:"14038920201",gender:"f",payment_method:"boleto",birth_date:"20/05/2000")
Student.create!(name:"Thiago",cpf:"17022233390",gender:"m",payment_method:"cartao",birth_date:"10/01/2002")

Instituion.create!(name:"uff",cnpj:"12345678900",type_status:"universidade" )
Instituion.create!(name:"fatesp",cnpj:"12333678901",type_status:"universidade" )
Instituion.create!(name:"escola",cnpj:"12345678902",type_status:"escola" )
Instituion.create!(name:"creche",cnpj:"12345678903",type_status:"creche" )

Enrollment.create!(name_course:"Computação",number_invoice:1,total_course_fee:2000,expiration_day:10,student_id:1,instituion_id:1)
Enrollment.create!(name_course:"Creche do papai",number_invoice:2,total_course_fee:2500,expiration_day:25,student_id:2,instituion_id:4)
Enrollment.create!(name_course:"Nono Ano",number_invoice:4,total_course_fee:12000,expiration_day:15,student_id:3,instituion_id:3)




