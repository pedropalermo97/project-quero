FROM ruby:3.0.0
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /billinho
WORKDIR /billinho
COPY Gemfile /billinho/Gemfile
COPY Gemfile.lock /billinho/Gemfile.lock

RUN bundle install
RUN bundle update --bundler

COPY . /billinho
