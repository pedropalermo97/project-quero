Rails.application.routes.draw do
  
  post 'students', to: 'student#create'
  get 'students', to: 'student#index'
  get 'students/:id', to:'student#show'
  put 'students/:id', to:'student#update'
  delete 'students/:id', to:'student#destroy'

  post 'instituions', to: 'instituion#create'
  get 'instituions', to: 'instituion#index'
  get 'instituions/:id', to: 'instituion#show'
  put 'instituions/:id', to: 'instituion#update'
  delete 'instituions/:id', to: 'instituion#destroy'

  post 'enrollments', to: 'enrollment#create'
  get 'enrollments', to: 'enrollment#index'
  get 'enrollments/:id', to: 'enrollment#show'
  put 'enrollments/:id', to: 'enrollment#update'
  delete 'enrollments/:id', to: 'enrollment#destroy'

  post 'invoices', to: 'invoice#create'
  get 'invoices', to: 'invoice#index'
  get 'invoices/:id', to: 'invoice#show'
  put 'invoices/:id', to: 'invoice#update'
  delete 'invoices/:id', to: 'invoice#destroy'

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
